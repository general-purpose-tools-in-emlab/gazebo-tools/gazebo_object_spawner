# gazebo_object_spawner 
This repository is arrangement of object on Gazebo.

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**

*   [Preparation](#preparation)
*   [Execution](#execution)
*   [Files](#files)
*   [References](#References)


## Preparation
You need to set parameter and filepath, etc in `__init__.py`.  
The following are the items to set.

* Object file path
* Object file name
* Object list
* Target place name, position (you need to check the position which show placement of objects.)

For example, you can use the following object models.  
[frontiers2021_gazebo_worlds](https://github.com/Shoichi-Hasegawa0628/frontiers2021_gazebo_worlds)

## Execution
~~~
roslaunch gazebo_object_spawner gazebo_object_spawner_default.launch
~~~

Please input word: `spawn` or `delete` will appear on the terminal,  
so enter `spawn` to place objects and `delete` to delete them.

## Files
 - `README.md`: Read me file (This file)

 - `gazebo_object_spawner_default.launch`
 
 - `main.py`

 - `__init__.py`: Code for initial setting (PATH and parameters)
 
## Reference
